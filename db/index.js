const themes = require('./themes.js');
const webinars = require('./webinars.js');
const realthemes = require('./realThemes');
const library = require('./library');

module.exports = {
	themes,
	webinars,
	realthemes,
	library
}
