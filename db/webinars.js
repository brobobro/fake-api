module.exports = [
	{
		id: 0,
		heading: 'The First Webinar',
		desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus.",
		duration: 120,
		date: '10.10.10',
		shown: false,
		img: "https://avatanplus.com/files/resources/original/57ea75fa968a11576bdcdaf9.png",
		addLink: 'https://calendar.google.com/event?action=TEMPLATE&tmeid=M2MzNGs3cmc1ajY1a3I4MWFxbTVkbWpyODggYW5keWN5YmVycmllc0Bt&tmsrc=andycyberries%40gmail.com',
		people: [
			{
				name: 'Todd Papers',
				desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				image: 'https://vignette.wikia.nocookie.net/fallout/images/a/a8/Todd_Howard.jpg/revision/latest/scale-to-width-down/250?cb=20120322172240'
			},
			{
				name: 'Todd Papers',
				desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				image: 'https://vignette.wikia.nocookie.net/fallout/images/a/a8/Todd_Howard.jpg/revision/latest/scale-to-width-down/250?cb=20120322172240'
			}
		]
	},
	{
		id: 1,
		heading: 'The Middle Webinar',
		desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus.",
		duration: 120,
		date: '10.10.10',
		shown: 'inprogress',
		img: "https://avatanplus.com/files/resources/original/57ea75fa968a11576bdcdaf9.png",
		addLink: 'https://calendar.google.com/event?action=TEMPLATE&tmeid=M2MzNGs3cmc1ajY1a3I4MWFxbTVkbWpyODggYW5keWN5YmVycmllc0Bt&tmsrc=andycyberries%40gmail.com',
		people: [
			{
				name: 'Todd Papers',
				desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				image: 'https://vignette.wikia.nocookie.net/fallout/images/a/a8/Todd_Howard.jpg/revision/latest/scale-to-width-down/250?cb=20120322172240'
			},
			{
				name: 'Todd Papers',
				desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				image: 'https://vignette.wikia.nocookie.net/fallout/images/a/a8/Todd_Howard.jpg/revision/latest/scale-to-width-down/250?cb=20120322172240'
			}
		]
	},
	{
		id: 2,
		heading: 'The Last Webinar',
		desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus.",
		duration: 120,
		date: '10.10.10',
		shown: true,
		img: "https://avatanplus.com/files/resources/original/57ea75fa968a11576bdcdaf9.png",
		addLink: 'https://calendar.google.com/event?action=TEMPLATE&tmeid=M2MzNGs3cmc1ajY1a3I4MWFxbTVkbWpyODggYW5keWN5YmVycmllc0Bt&tmsrc=andycyberries%40gmail.com',
		people: [
			{
				name: 'Todd Papers',
				desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				image: 'https://vignette.wikia.nocookie.net/fallout/images/a/a8/Todd_Howard.jpg/revision/latest/scale-to-width-down/250?cb=20120322172240'
			},
			{
				name: 'Todd Papers',
				desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur ipsa impedit dicta porro, numquam officiis dolorem! Sapiente possimus molestias sed saepe, harum sit, mollitia ad ipsam hic molestiae doloribus temporibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				image: 'https://vignette.wikia.nocookie.net/fallout/images/a/a8/Todd_Howard.jpg/revision/latest/scale-to-width-down/250?cb=20120322172240'
			}
		]
	}
]
